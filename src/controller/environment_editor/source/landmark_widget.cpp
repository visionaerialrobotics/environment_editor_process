/*!*******************************************************************************************
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
/*
  LandmarkWidget
  @author  Jorge Luis Pascual, Carlos Valencia.
  @date    07-2017
  @version 2.0
*/
#include "../include/landmark_widget.h"

LandmarkWidget::LandmarkWidget(QWidget* parent, ObjectController::Landmark landmark, int i)
  : QWidget(parent), ui(new Ui::LandmarkWidget)
{
  ui->setupUi(this);
  this->landmark = landmark;
  this->internal_id = i;
  spin_id = ui->spin_id;
  spin_x_size = ui->spin_x_size;
  spin_y_size = ui->spin_y_size;
  spin_x_coor = ui->spin_x_coor;
  spin_y_coor = ui->spin_y_coor;
  spin_aruco0_id = ui->spin_aruco0_id;
  spin_aruco1_id = ui->spin_aruco1_id;

  degrees = ui->degrees;

  accept = ui->accept;

  spin_id->setValue(landmark.id);
  spin_x_size->setValue(landmark.x_size);
  spin_y_size->setValue(landmark.y_size);
  spin_x_coor->setValue(landmark.x_coor);
  spin_y_coor->setValue(landmark.y_coor);

  spin_aruco0_id->setValue(landmark.aruco[0].id);
  spin_aruco1_id->setValue(landmark.aruco[1].id);

  degrees->setValue(landmark.degrees);

  QObject::connect(accept, SIGNAL(clicked()), this, SLOT(acceptButton()));
  QObject::connect(spin_id, SIGNAL(valueChanged(int)), this, SLOT(valueChanged()));
  QObject::connect(spin_x_size, SIGNAL(valueChanged(double)), this, SLOT(valueChanged()));
  QObject::connect(spin_y_size, SIGNAL(valueChanged(double)), this, SLOT(valueChanged()));
  QObject::connect(spin_x_coor, SIGNAL(valueChanged(double)), this, SLOT(valueChanged()));
  QObject::connect(spin_y_coor, SIGNAL(valueChanged(double)), this, SLOT(valueChanged()));
  QObject::connect(spin_aruco0_id, SIGNAL(valueChanged(int)), this, SLOT(valueChanged()));
  QObject::connect(spin_aruco1_id, SIGNAL(valueChanged(int)), this, SLOT(valueChanged()));
  QObject::connect(degrees, SIGNAL(valueChanged(double)), this, SLOT(valueChanged()));
}

LandmarkWidget::~LandmarkWidget()
{
  delete ui;
}

void LandmarkWidget::acceptButton()
{
  Q_EMIT(closed());
  close();
}

void LandmarkWidget::valueChanged()
{
  ObjectController::Landmark landmark = this->landmark;
  landmark.id = spin_id->value();
  landmark.x_size = spin_x_size->value();
  landmark.y_size = spin_y_size->value();
  landmark.x_coor = spin_x_coor->value();
  landmark.y_coor = spin_y_coor->value();
  landmark.aruco[0].id = spin_aruco0_id->value();
  landmark.aruco[1].id = spin_aruco1_id->value();
  landmark.degrees = degrees->value();
  Q_EMIT(landmarkChanged(landmark, this->internal_id));
}
