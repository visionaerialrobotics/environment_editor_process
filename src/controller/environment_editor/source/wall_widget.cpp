/*!*******************************************************************************************
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
/*
  WallWidget
  @author  Jorge Luis Pascual, Carlos Valencia.
  @date    07-2017
  @version 2.0
*/
#include "../include/wall_widget.h"

WallWidget::WallWidget(QWidget* parent, ObjectController::Wall w, int i) : QWidget(parent), ui(new Ui::WallWidget)
{
  ui->setupUi(this);
  this->w = w;
  this->internal_id = i;
  spin_id = ui->spin_id;
  spin_x_size = ui->spin_x_size;
  spin_y_size = ui->spin_y_size;
  spin_x_coor = ui->spin_x_coor;
  spin_y_coor = ui->spin_y_coor;
  spin_degrees = ui->spin_degrees;
  description = ui->description;
  virtual_description = ui->virtual_description;
  accept = ui->accept;

  spin_id->setValue(w.id);
  spin_x_size->setValue(w.x_size);
  spin_y_size->setValue(w.y_size);
  spin_x_coor->setValue(w.x_coor);
  spin_y_coor->setValue(w.y_coor);
  spin_degrees->setValue(w.degrees);
  description->setText(QString::fromUtf8(w.description.c_str()));
  virtual_description->setText(QString::fromUtf8(w.virtual_description.c_str()));

  QObject::connect(accept, SIGNAL(clicked()), this, SLOT(acceptButton()));
  QObject::connect(spin_id, SIGNAL(valueChanged(int)), this, SLOT(valueChanged()));
  QObject::connect(spin_x_size, SIGNAL(valueChanged(double)), this, SLOT(valueChanged()));
  QObject::connect(spin_y_size, SIGNAL(valueChanged(double)), this, SLOT(valueChanged()));
  QObject::connect(spin_x_coor, SIGNAL(valueChanged(double)), this, SLOT(valueChanged()));
  QObject::connect(spin_y_coor, SIGNAL(valueChanged(double)), this, SLOT(valueChanged()));
  QObject::connect(spin_degrees, SIGNAL(valueChanged(double)), this, SLOT(valueChanged()));
  QObject::connect(description, SIGNAL(textEdited(const QString)), this, SLOT(valueChanged()));
  QObject::connect(virtual_description, SIGNAL(textEdited(const QString)), this, SLOT(valueChanged()));
}

WallWidget::~WallWidget()
{
  delete ui;
}

void WallWidget::acceptButton()
{
  Q_EMIT(closed());
  close();
}

void WallWidget::valueChanged()
{
  ObjectController::Wall w = this->w;
  w.id = spin_id->value();
  w.x_size = spin_x_size->value();
  w.y_size = spin_y_size->value();
  w.x_coor = spin_x_coor->value();
  w.y_coor = spin_y_coor->value();
  w.degrees = spin_degrees->value();
  w.description = description->text().toUtf8().constData();
  w.virtual_description = virtual_description->text().toUtf8().constData();
  Q_EMIT(wallChanged(w, this->internal_id));
}
