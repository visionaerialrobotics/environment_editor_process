/*!*******************************************************************************************
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
/*
  ToolWidget
  @author  Jorge Luis Pascual, Carlos Valencia.
  @date    07-2017
  @version 2.0
*/
#include "../include/tool_widget.h"

ToolWidget::ToolWidget(QWidget* parent) : QWidget(parent), ui(new Ui::ToolWidget)
{
  ui->setupUi(this);
  button1 = ui->b1;
  button2 = ui->b2;
  button3 = ui->b3;
  button4 = ui->b4;
  button5 = ui->b5;
  button6 = ui->b6;
  button7 = ui->b7;
  mapper = new QSignalMapper(this);
  QObject::connect(button1, SIGNAL(clicked()), mapper, SLOT(map()));
  mapper->setMapping(button1, button1);
  QObject::connect(button2, SIGNAL(clicked()), mapper, SLOT(map()));
  mapper->setMapping(button2, button2);
  QObject::connect(button3, SIGNAL(clicked()), mapper, SLOT(map()));
  mapper->setMapping(button3, button3);
  QObject::connect(button4, SIGNAL(clicked()), mapper, SLOT(map()));
  mapper->setMapping(button4, button4);
  QObject::connect(button5, SIGNAL(clicked()), mapper, SLOT(map()));
  mapper->setMapping(button5, button5);
  QObject::connect(button6, SIGNAL(clicked()), mapper, SLOT(map()));
  mapper->setMapping(button6, button6);
  QObject::connect(button7, SIGNAL(clicked()), this, SLOT(onClickedStikyButton()));
  QObject::connect(mapper, SIGNAL(mapped(QWidget*)), this, SLOT(onClickedButton(QWidget*)));
  setFocusPolicy(Qt::NoFocus);
}

ToolWidget::~ToolWidget()
{
  delete ui;
  delete mapper;
}

void ToolWidget::onClickedButton(QWidget* b)
{
  if (last_pressed != NULL)
  {
    last_pressed->setChecked(false);
  }
  QToolButton* button = (QToolButton*)b;
  button->setChecked(true);
  switch (atoi(b->objectName().toStdString().substr(1, 1).c_str()))
  {
    case 1:
    {
      QPixmap p(":/images/images/cursor-drone.png");
      QCursor q(p.scaled(40, 40), 0, 0);
      QApplication::setOverrideCursor(q);
      break;
    }
    case 2:
    {
      QPixmap p(":/images/images/cursor-pole.png");
      QCursor q(p.scaled(40, 40), 0, 0);
      QApplication::setOverrideCursor(q);
      break;
    }
    case 3:
    {
      QPixmap p(":/images/images/cursor-landmark.png");
      QCursor q(p.scaled(40, 40), 0, 0);
      QApplication::setOverrideCursor(q);
      break;
    }
    case 4:
    {
      QPixmap p(":/images/images/cursor-cursor.png");
      QCursor q(p.scaled(20, 20), 0, 0);
      QApplication::setOverrideCursor(q);
      break;
    }
    case 5:
    {
      QPixmap p(":/images/images/cursor-cursor.png");
      QCursor q(p.scaled(20, 20), 0, 0);
      QApplication::setOverrideCursor(q);
      break;
    }
    case 6:
    {
      QApplication::setOverrideCursor(Qt::OpenHandCursor);
      break;
    }
    default:
    {
      QPixmap p(":/images/images/cursor-cursor.png");
      QCursor q(p.scaled(20, 20), 0, 0);
      QApplication::setOverrideCursor(q);
      break;
    }
  }
  last_pressed = button;
}

QToolButton* ToolWidget::getTool()
{
  if (last_pressed != NULL)
    return last_pressed;
  return NULL;
}

void ToolWidget::onClickedStikyButton()
{
  sticky = !sticky;
}

bool ToolWidget::isSticky()
{
  return sticky;
}
void ToolWidget::unsetTool()
{
  last_pressed = NULL;
  sticky = false;
  button1->setChecked(false);
  button2->setChecked(false);
  button3->setChecked(false);
  button4->setChecked(false);
  button5->setChecked(false);
  button6->setChecked(false);
  button7->setChecked(false);
  QApplication::setOverrideCursor(Qt::ArrowCursor);
}
