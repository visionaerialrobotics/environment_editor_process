/*!*******************************************************************************************
 *  \file       wall_widget.h
 *  \brief      WallWidget definition file.
 *  \details    WallWidget is the widget used for the walls representation.
 *  \author     Jorge Luis Pascual, Carlos Valencia.
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
#ifndef WALL_WIDGET_H
#define WALL_WIDGET_H

#include <QWidget>
#include <QObject>
#include <iostream>
#include <QSignalMapper>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QLineEdit>
#include "ui_wall_widget.h"
#include "object_controller.h"

namespace Ui
{
class WallWidget;
}

class WallWidget : public QWidget
{
  Q_OBJECT

public:
  explicit WallWidget(QWidget* parent, ObjectController::Wall w, int i);
  ~WallWidget();

private:
  Ui::WallWidget* ui;
  int internal_id;
  ObjectController::Wall w;
  QSpinBox* spin_id;
  QDoubleSpinBox* spin_x_size;
  QDoubleSpinBox* spin_y_size;
  QDoubleSpinBox* spin_x_coor;
  QDoubleSpinBox* spin_y_coor;
  QDoubleSpinBox* spin_degrees;
  QLineEdit* description;
  QLineEdit* virtual_description;
  QPushButton* accept;

public Q_SLOTS:

  /*!********************************************************************************************************************
   *  \brief      This slot is executed when the accept button is clicked
   **********************************************************************************************************************/
  void acceptButton();

  /*!********************************************************************************************************************
   *  \brief      This method is executed whenever the wall's data values are changed
   **********************************************************************************************************************/
  void valueChanged();

Q_SIGNALS:

  /*!********************************************************************************************************************
   *  \brief      This signal is emitted when the wall edition menu is closed
   **********************************************************************************************************************/
  void closed();

  /*!********************************************************************************************************************
   *  \brief      This signal is emitted when the wall's data values are changed
   **********************************************************************************************************************/
  void wallChanged(ObjectController::Wall wall, int i);
};

#endif
