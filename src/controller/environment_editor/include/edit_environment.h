/*!*******************************************************************************************
 *  \file       environment_widget.h
 *  \brief      EditEnvironment definition file.
 *  \details    EditEnvironment is the widget used for the map edition management.
 *  \author     Jorge Luis Pascual, Carlos Valencia.
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
#ifndef ENVIRONMENT_WIDGET_H
#define ENVIRONMENT_WIDGET_H

#include <ros/ros.h>
#include "droneMsgsROS/configurationFolder.h"
#include "aerostack_msgs/WindowIdentifier.h"

#include <QFileDialog>
#include <QMessageBox>
#include <QWidget>
#include <QGridLayout>
#include <QGraphicsView>
#include <QSignalMapper>
#include <QProcess>
#include <QAbstractButton>
#include <QScreen>

#include "ui_edit_environment.h"
#include "tool_widget.h"
#include "new_map_menu.h"
#include "object_controller.h"
#include "drone_widget.h"
#include "wall_widget.h"
#include "pole_widget.h"
#include "landmark_widget.h"
#include "config_file_manager.h"

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include "fstream"

class EnvironmentViewerConnection;
class ReceivedDataProcessor;

namespace Ui
{
class EditEnvironment;
}

class EditEnvironment : public QWidget
{
  Q_OBJECT

public:
  explicit EditEnvironment(int argc, char** argv, QWidget* parent = 0);
  ~EditEnvironment();

private:
  Ui::EditEnvironment* ui;

  ros::NodeHandle n;
  ros::Publisher window_closed_publ;
  ros::Subscriber window_opened_subs;
  ros::ServiceClient configuration_folder_client;
  droneMsgsROS::configurationFolder::Request req;
  droneMsgsROS::configurationFolder::Response res;
  aerostack_msgs::WindowIdentifier windowClosedMsgs;
  aerostack_msgs::WindowIdentifier windowOpenedMsgs;

  boost::property_tree::ptree root;

  ToolWidget* tool_widget;
  ObjectController* object_controller;
  NewMapMenu* new_map_menu;
  ConfigFileManager* config_file_manager;

  QLabel* object_label;
  QLabel* tools_label;
  QWidget* widget_config = NULL;
  QPushButton* new_map_button;
  QPushButton* save_map_button;
  QPushButton* load_map_button;
  QPushButton* import_map_button;
  QPushButton* export_map_button;
  QPushButton* cancel_map_button;
  QPushButton* accept_map_button;
  QGridLayout* layout;
  QGridLayout* layout_buttons;
  QAbstractButton* m_save_button;
  QAbstractButton* m_cancel_button;
  QAbstractButton* m_dont_save_button;
  QMessageBox map_file_msg;

  std::string my_stack_directory;
  std::string configuration_folder;
  std::string configuration_folder_service;
  std::string drone_id_namespace;
  std::string initiate_behaviors;
  std::string window_opened_topic;
  std::string window_closed_topic;
  std::string rosnamespace;

  bool null = true;

  /*!************************************************************************
   *  \brief   Activated when a window is closed.
   ***************************************************************************/
  void windowOpenCallback(const aerostack_msgs::WindowIdentifier& msg);
  /*!************************************************************************
   *  \brief  Kills the process
   ***************************************************************************/
  void killMe();

public Q_SLOTS:
  /*!********************************************************************************************************************
   *  \brief      This method connects the ObjectController signals to the BehaviorTree slots.
   **********************************************************************************************************************/
  void connectEnvironmentWidgetObjectController();

  /*!********************************************************************************************************************
   *  \brief      This slot is executed when the user wants to create a new map. It allows the user to select the
   *dimensions of the map.
   **********************************************************************************************************************/
  void createNewMapMenu();

  /*!********************************************************************************************************************
   *  \brief      This slot is executed when the user wants to create a new drone object.
   **********************************************************************************************************************/
  void createDroneMenu(ObjectController::Drone drone, int i);

  /*!********************************************************************************************************************
   *  \brief      This slot is executed when the user wants to create a new wall object.
   **********************************************************************************************************************/
  void createWallMenu(ObjectController::Wall wall, int i);

  /*!********************************************************************************************************************
   *  \brief      This slot is executed when the user wants to create a new pole object.
   **********************************************************************************************************************/
  void createPoleMenu(ObjectController::Pole pole, int i);

  /*!********************************************************************************************************************
   *  \brief      This slot is executed when the user wants to create a new landmark object.
   **********************************************************************************************************************/
  void createLandmarkMenu(ObjectController::Landmark land, int i);

  /*!********************************************************************************************************************
   *  \brief      This slot is executed when the map is exported or canceled.
   **********************************************************************************************************************/
  void deleteMenu();

  /*!********************************************************************************************************************
   *  \brief      This slot is executed when the user wants to save the map to a file.
   **********************************************************************************************************************/
  void saveMap();

  /*!********************************************************************************************************************
   *  \brief      This slot is executed when the user wants to load a map from a file.
   **********************************************************************************************************************/
  void loadMap();

  /*!********************************************************************************************************************
   *  \brief      This slot is executed when the user accepts the edition of the map. It will export the map in the
   *Aerostack config files.
   **********************************************************************************************************************/
  void exportMap();

  /*!********************************************************************************************************************
   *  \brief      This slot is executed when the EditEnvironment is created or when the user wants to import the map
   *from the Aerostack config files.
   **********************************************************************************************************************/
  void importMap();

  /*!********************************************************************************************************************
   *  \brief      This slot is executed when the user clicks the Cancel button within the edition mode.
   **********************************************************************************************************************/
  void cancelMap();

  /*!********************************************************************************************************************
   *  \brief      This slot is executed when the user clicks the Clear button
   **********************************************************************************************************************/
  void clearTrajectories();

  /*!********************************************************************************************************************
   *  \brief      This method notifies main window that the widget was closed
   *********************************************************************************************************************/
  void closeEvent(QCloseEvent* event);

Q_SIGNALS:
};
#endif  // ENVIRONMENT_WIDGET_H
